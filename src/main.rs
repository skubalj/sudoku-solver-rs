use crate::grid::Grid;
use cell::SudokuCell;
use druid::{AppLauncher, Data, Lens, WindowDesc};

#[macro_use]
extern crate bitflags;

mod cell;
mod grid;
mod interface;

#[derive(Debug, Default, Clone, Data, Lens)]
struct AppState {
    cell: SudokuCell,
}

fn main() {
    let window = WindowDesc::new(interface::build_widgets)
        .title("Sudoku Solver")
        .window_size((250.0, 250.0))
        .resizable(false);
    let res = AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(Grid::new());
    if let Err(e) = res {
        eprintln!("{:?}", e);
    }
}
