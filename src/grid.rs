use crate::cell::SudokuCell;
use druid::{Data, Lens};
use std::cell::RefCell;
use std::rc::Rc;

/// A grid of all 81 sudoku cells
#[derive(Debug, Clone, Data)]
pub struct Grid(Rc<RefCell<[SudokuCell; 81]>>);

impl Grid {
    /// Create a new blank grid
    pub fn new() -> Self {
        Grid(Rc::new(RefCell::new([SudokuCell::default(); 81])))
    }

    /// Clear the grid, setting all cells to `ANY`
    pub fn clear(&mut self) {
        for cell in self.0.borrow_mut().iter_mut() {
            *cell = SudokuCell::ANY;
        }
    }
}

/// A lens to get a single cell from the Grid of all cells
#[derive(Debug, Clone, Copy)]
pub struct CellLens(pub usize, pub usize);

impl Lens<Grid, SudokuCell> for CellLens {
    fn with<V, F: FnOnce(&SudokuCell) -> V>(&self, data: &Grid, f: F) -> V {
        let CellLens(row, col) = *self;
        f(&data.0.borrow()[9 * row + col])
    }

    fn with_mut<V, F: FnOnce(&mut SudokuCell) -> V>(&self, data: &mut Grid, f: F) -> V {
        let CellLens(row, col) = *self;
        f(&mut data.0.borrow_mut()[9 * row + col])
    }
}
