use crate::cell::CellFormatter;
use crate::grid::{CellLens, Grid};
use druid::widget::{Button, Flex, LensWrap, TextBox};
use druid::TextAlignment;
use druid::Widget;

/// The space between cells in a minor gap
const MINOR_SPACE: f64 = 1.0;

/// The space between cells in a major gap
const MAJOR_SPACE: f64 = 5.0;

pub fn build_widgets() -> impl Widget<Grid> {
    Flex::column()
        .with_flex_child(build_grid(), 1.0)
        .with_child(Button::new("Clear Grid").on_click(|_ctx, grid: &mut Grid, _env| grid.clear()))
}

pub fn build_grid() -> impl Widget<Grid> {
    let mut grid = Flex::column().with_flex_child(build_row(0), 1.0);

    for i in 1..9 {
        grid.add_spacer(if i % 3 == 0 { MAJOR_SPACE } else { MINOR_SPACE });
        grid.add_flex_child(build_row(i), 1.0);
    }

    grid
}

pub fn build_row(n: usize) -> impl Widget<Grid> {
    let mut row = Flex::row().with_flex_child(
        LensWrap::new(
            TextBox::new()
                .with_text_alignment(TextAlignment::Center)
                .with_text_size(24.0)
                .with_formatter(CellFormatter()),
            CellLens(n, 0),
        ),
        1.0,
    );

    for i in 1..9 {
        row.add_spacer(if i % 3 == 0 { MAJOR_SPACE } else { MINOR_SPACE });
        row.add_flex_child(
            LensWrap::new(
                TextBox::new()
                    .with_text_alignment(TextAlignment::Center)
                    .with_text_size(24.0)
                    .with_formatter(CellFormatter()),
                CellLens(n, i),
            ),
            1.0,
        );
    }

    row
}
