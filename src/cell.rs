use druid::text::format::{Formatter, Validation, ValidationError};
use druid::text::Selection;
use druid::Data;
use std::error::Error;
use std::fmt::{self, Display};
use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct CellFormatter();

impl Formatter<SudokuCell> for CellFormatter {
    fn format(&self, value: &SudokuCell) -> String {
        value.to_string()
    }

    fn validate_partial_input(&self, input: &str, _sel: &Selection) -> Validation {
        match input {
            "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "" => Validation::success(),
            _ => {
                let new_text = match input.chars().last() {
                    Some(x) if matches!(x, '1'..='9') => x.to_string(),
                    _ => String::new(),
                };
                Validation::failure(ParseSudokuCellError::new(input)).change_text(new_text)
            }
        }
    }

    fn value(&self, input: &str) -> Result<SudokuCell, ValidationError> {
        input.parse().map_err(ValidationError::new)
    }
}

#[derive(Debug, Clone)]
pub struct ParseSudokuCellError {
    input: String,
}

impl ParseSudokuCellError {
    pub fn new(val: impl ToString) -> Self {
        Self {
            input: val.to_string(),
        }
    }
}

impl Display for ParseSudokuCellError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Received value of: {}", self.input)
    }
}

impl Error for ParseSudokuCellError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }

    fn description(&self) -> &str {
        "Unable to convert str value to SudokuCell"
    }

    fn cause(&self) -> Option<&dyn Error> {
        None
    }
}

bitflags! {
    #[derive(Data)]
    pub struct SudokuCell: u16 {
        const ONE   = 0b0_0000_0001;
        const TWO   = 0b0_0000_0010;
        const THREE = 0b0_0000_0100;
        const FOUR  = 0b0_0000_1000;
        const FIVE  = 0b0_0001_0000;
        const SIX   = 0b0_0010_0000;
        const SEVEN = 0b0_0100_0000;
        const EIGHT = 0b0_1000_0000;
        const NINE  = 0b1_0000_0000;
        const ANY   = 0b1_1111_1111;
    }
}

impl Default for SudokuCell {
    fn default() -> Self {
        Self::ANY
    }
}

impl Display for SudokuCell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.value() {
            Some(value) => write!(f, "{}", value),
            None => write!(f, ""),
        }
    }
}

impl SudokuCell {
    pub fn value(&self) -> Option<u8> {
        match *self {
            Self::ONE => Some(1),
            Self::TWO => Some(2),
            Self::THREE => Some(3),
            Self::FOUR => Some(4),
            Self::FIVE => Some(5),
            Self::SIX => Some(6),
            Self::SEVEN => Some(7),
            Self::EIGHT => Some(8),
            Self::NINE => Some(9),
            _ => None,
        }
    }
}

impl FromStr for SudokuCell {
    type Err = ParseSudokuCellError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "1" => Ok(Self::ONE),
            "2" => Ok(Self::TWO),
            "3" => Ok(Self::THREE),
            "4" => Ok(Self::FOUR),
            "5" => Ok(Self::FIVE),
            "6" => Ok(Self::SIX),
            "7" => Ok(Self::SEVEN),
            "8" => Ok(Self::EIGHT),
            "9" => Ok(Self::NINE),
            "" => Ok(Self::ANY),
            _ => Err(ParseSudokuCellError::new(s)),
        }
    }
}
